import React from "react";
import "./App.scss";
import UserPage from "./components/UserPage";
import LoginPage from "./LoginPage";
import ThemeProvider from "./theme/ThemeProvider";

function App() {
  return (
    <ThemeProvider>
      <UserPage />
      <LoginPage />
    </ThemeProvider>
  );
}

export default App;
