import React from "react";
import TestContent from "./TestContent";
import Dashboard from "./Dashboard";

export interface UserPageProps {}
const UserPage = (props: UserPageProps) => {
  return (
    <Dashboard>
      <TestContent />
    </Dashboard>
  );
};
export default UserPage;
