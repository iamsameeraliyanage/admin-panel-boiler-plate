import { Avatar } from "@material-ui/core";
import React from "react";
import "./TableAvatar.scss";
interface TableIconAvatarProps {
  backgroundColor?: string;
  avatarIcon?: JSX.Element;
  title?: string;
}
/**
 * no usages yet,
 * shouldn't be removed
 */
export const TableIconAvatar = ({
  backgroundColor,
  avatarIcon,
  title,
}: TableIconAvatarProps) => {
  return (
    <div className="avatar-item">
      <Avatar
        className={"table-avatar"}
        style={{
          height: "20px",
          width: "20px",
          backgroundColor: backgroundColor,
        }}
      >
        {avatarIcon}
      </Avatar>
      {title}
    </div>
  );
};
