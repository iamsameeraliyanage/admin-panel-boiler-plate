import { Avatar } from "@material-ui/core";
import React from "react";
import "./TableAvatar.scss";
interface TableAvatarProps {
  title?: string;
  subTitle?: string;
  children?: JSX.Element | JSX.Element[];
}
/**
 * no usages yet,
 * shouldn't be removed
 */
export const TableAvatar = ({
  title,
  children,
  subTitle,
}: TableAvatarProps) => {
  return (
    <div className="avatar-item">
      <Avatar className="table-avatar">
        {title && title.charAt(0).toUpperCase()}
      </Avatar>
      <div className="avatar-detail">
        <div className="main-text">{title}</div>
        {subTitle && <div className="sub-text"> {subTitle}</div>}
        {children}
      </div>
    </div>
  );
};
