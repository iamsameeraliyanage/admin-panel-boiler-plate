import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import { TableAvatar } from "./TableAvatar/TableAvatar";
import TableHeader from "./TableHeader/TableHeader";
import TableActions from "./TableActions/TableActions";

const styles = (theme: Theme) =>
  createStyles({
    paper: {
      maxWidth: 936,
      margin: "auto",
      overflow: "hidden",
    },
    searchBar: {
      borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
    },
    searchInput: {
      fontSize: theme.typography.fontSize,
    },
    block: {
      display: "block",
    },
    addUser: {
      marginRight: theme.spacing(1),
    },
    contentWrapper: {
      margin: "40px 16px",
    },
  });

export interface TestContentProps extends WithStyles<typeof styles> {}

function TestContent(props: TestContentProps) {
  const { classes } = props;
  const [searchText, setSearchInput] = useState("");

  console.log("====================================");
  console.log(searchText);
  console.log("====================================");
  const handleRemoveClick = () => {
    console.log("====================================");
    console.log("remove");
    console.log("====================================");
  };
  return (
    <Paper className={classes.paper}>
      <TableHeader setSearchInput={setSearchInput} />
      <div className="table-box">
        <TableContainer>
          <Table aria-label="simple table">
            <TableBody>
              <TableRow>
                <TableCell>
                  <TableAvatar title={"email@email.com"} />
                </TableCell>
                <TableCell>user role</TableCell>

                <TableCell align="right">
                  <TableActions handleRemoveClick={handleRemoveClick} />
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </Paper>
  );
}

export default withStyles(styles)(TestContent);
