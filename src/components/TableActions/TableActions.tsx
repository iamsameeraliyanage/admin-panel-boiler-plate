import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import { IconButton } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

export interface TableActionsProps {
  handleRemoveClick: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    removeButton: {
      color: "#d80202",
    },
  })
);

const TableActions = ({ handleRemoveClick }: TableActionsProps) => {
  const classes = useStyles();
  return (
    <>
      <IconButton
        size="small"
        aria-label="settings"
        onClick={handleRemoveClick}
        className={classes.removeButton}
      >
        <DeleteIcon />
      </IconButton>
    </>
  );
};
export default TableActions;
