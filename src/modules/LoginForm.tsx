import { Button, TextField } from "@material-ui/core";
import React from "react";

interface Props {
  onSubmit(): void;
}

const LoginForm = ({ onSubmit }: Props) => {
  return (
    <div className="form-wrapper">
      <form className="form" onSubmit={onSubmit}>
        <div className="form-group">
          <TextField fullWidth label="Email" type="email" variant="outlined" />
        </div>
        <div className="form-group">
          <TextField
            fullWidth
            label="Password"
            type="password"
            variant="outlined"
          />
        </div>
        <div className="form-group mb-4">
          <div className="form-txt-wrap">
            <div className="form-txt">Forgot your password?</div>
          </div>
        </div>
        <div className="form-group btn-form-group">
          <Button
            className="w-100"
            color="primary"
            size="large"
            type="submit"
            variant="contained"
          >
            Sign in
          </Button>
        </div>
      </form>
    </div>
  );
};
export default LoginForm;
